import logging

import requests
from flask_restful import Resource, reqparse

import config
from controllers.rentals import check_rental_exists
from database import db, Rental

payments_parser = reqparse.RequestParser()
payments_parser.add_argument('credit_card_number', type=str, required=True, help='Credit Card number cannot be blank!')


class RentalPaymentController(Resource):
    @check_rental_exists
    def post(self, rental_id):
        args = payments_parser.parse_args()
        credit_card_number = args['credit_card_number']
        rental = Rental.query.get(rental_id)
        if rental.paid:
            return {"status": "rental already paid"}, 400
        
        payment_response = requests.post(config.payments_lambda_url, json={"credit_card_number": credit_card_number})
        if payment_response.status_code == 200:
            logging.debug("Payment was successful")
            rental.paid = 1
            db.session.commit()
            return {"status": "rental paid"}, 201
        else:
            logging.debug("Payment was not successful, API response", payment_response.text)
            return {"status": "rental payment error"}, 400
