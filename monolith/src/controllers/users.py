from flask_restful import Resource, fields, marshal_with, reqparse, abort
from functools import wraps
from database import db, User

user_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'email': fields.String,
}

parser = reqparse.RequestParser()
parser.add_argument('name', type=str, required=True, help='User name cannot be blank!')
parser.add_argument('email', type=str, required=True, help='User email cannot be blank!')


def user_exists(func):
    @wraps(func)
    def wrapper(self, user_id, *args, **kwargs):
        user = User.query.filter_by(id=user_id).first()
        if user:
            return func(self, user, *args, **kwargs)
        else:
            return {'error': 'User not found'}, 404

    return wrapper


class UserController(Resource):

    @user_exists
    def get(self, user, user_id):
        return {'id': user.id, 'name': user.name}

    @user_exists
    def put(self, user, user_id):
        args = parser.parse_args()
        user.name = args['name']
        db.session.commit()
        return {'message': 'User updated', 'user': {'id': user.id, 'name': user.name}}

    @user_exists
    def delete(self, user, user_id):
        db.session.delete(user)
        db.session.commit()
        return {'message': 'User deleted'}


class UserListController(Resource):

    def get(self):
        users = User.query.all()
        return {'users': [{'id': user.id, 'name': user.name} for user in users]}

    def post(self):
        args = parser.parse_args()
        user = User(name=args['name'], email=args['email'])
        db.session.add(user)
        db.session.commit()
        return {'message': 'User added', 'user': {'id': user.id, 'name': user.name, 'name': user.email}}, 201
