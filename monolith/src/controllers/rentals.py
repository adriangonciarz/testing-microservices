from functools import wraps

from flask_restful import Resource, fields, marshal_with, reqparse, abort, inputs

from database import db, Rental

rental_fields = {
    'id': fields.Integer,
    'user_id': fields.Integer,
    'movie_id': fields.Integer,
    'start_date': fields.DateTime(dt_format='iso8601'),
    'end_date': fields.DateTime(dt_format='iso8601'),
    'paid': fields.Boolean
}

rental_parser = reqparse.RequestParser()
rental_parser.add_argument('user_id', type=int, required=True, help='User ID cannot be blank!')
rental_parser.add_argument('movie_id', type=int, required=True, help='Movie ID cannot be blank!')
rental_parser.add_argument('start_date', type=inputs.datetime_from_iso8601, required=True,
                           help='Start date cannot be blank!')
rental_parser.add_argument('end_date', type=inputs.datetime_from_iso8601, required=True,
                           help='End date cannot be blank!')


def check_rental_exists(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        rental_id = kwargs.get('rental_id', None)
        if rental_id:
            rental = Rental.query.get(rental_id)
            if not rental:
                abort(404, message="Rental {} doesn't exist".format(rental_id))
        return func(*args, **kwargs)

    return wrapper


class RentalController(Resource):
    @check_rental_exists
    @marshal_with(rental_fields)
    def get(self, rental_id):
        return Rental.query.get(rental_id)

    @check_rental_exists
    def delete(self, rental_id):
        rental = Rental.query.get(rental_id)
        db.session.delete(rental)
        db.session.commit()
        return {}, 200


class RentalListController(Resource):
    def get(self):
        rentals = Rental.query.all()
        return {'rentals': [{'id': rental.id, 'user': rental.user.name, 'movie': rental.movie.title,
                             'start_date': rental.start_date.isoformat(),
                             'end_date': rental.end_date.isoformat(), 'paid': rental.paid} for rental in rentals]}

    @marshal_with(rental_fields)
    def post(self):
        args = rental_parser.parse_args()
        rental = Rental(user_id=args['user_id'], movie_id=args['movie_id'], start_date=args['start_date'],
                        end_date=args['end_date'])
        db.session.add(rental)
        db.session.commit()
        return rental, 201
