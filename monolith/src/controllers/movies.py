from flask_restful import Resource, fields, marshal_with, reqparse, abort
from database import db, Movie
from functools import wraps

resource_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'info': fields.String,
    'release_year': fields.Integer,
    'renting_price': fields.Float
}

parser = reqparse.RequestParser()
parser.add_argument('title', type=str, required=True, help='Title cannot be blank!')
parser.add_argument('info', type=str, required=True, help='Info cannot be blank!')
parser.add_argument('release_year', type=str, required=True, help='Release Year cannot be blank!')
parser.add_argument('renting_price', type=float, required=True, help='Renting Price cannot be blank!')


# Decorator for checking movie existence
def check_movie_exists(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        movie_id = kwargs.get('movie_id', None)
        if movie_id:
            movie = Movie.query.get(movie_id)
            if not movie:
                abort(404, message="Movie {} doesn't exist".format(movie_id))
        return func(*args, **kwargs)

    return wrapper


class MovieController(Resource):
    @check_movie_exists
    @marshal_with(resource_fields)
    def get(self, movie_id):
        return Movie.query.get(movie_id)

    @check_movie_exists
    def delete(self, movie_id):
        movie = Movie.query.get(movie_id)
        db.session.delete(movie)
        db.session.commit()
        return {}, 200

    @check_movie_exists
    @marshal_with(resource_fields)
    def put(self, movie_id):
        args = parser.parse_args()
        movie = Movie.query.get(movie_id)
        movie.title = args['title']
        movie.info = args['info']
        movie.release_year = args['release_year']
        movie.renting_price = args['renting_price']
        db.session.commit()
        return movie, 200


class MovieListController(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return Movie.query.all()

    @marshal_with(resource_fields)
    def post(self):
        args = parser.parse_args()
        movie = Movie(title=args['title'], info=args['info'], release_year=args['release_year'],
                            renting_price=args['renting_price'])
        db.session.add(movie)
        db.session.commit()
        return movie, 201
