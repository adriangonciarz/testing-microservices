from flask_restful import Resource


class HelloController(Resource):
    def get(self):
        return """
        <h1> Welcome to Movie Rentals API</h1>
        <h3>Explore the endpoints</h3>
        <ul>
            <li><a href='http://127.0.0.1:5000/api/movies'>Movies</li>
        </ul>
        """
