from flask_restful import Resource, fields, marshal_with, reqparse, abort

from controllers.rentals import check_rental_exists
from database import Rental, Rating, db

rating_fields = {
    'id': fields.Integer,
    'score': fields.Integer,
    'user_id': fields.Integer,
    'movie_id': fields.Integer,
}

parser = reqparse.RequestParser()
parser.add_argument('user_id', type=int, required=True, help='User ID cannot be blank!')
parser.add_argument('rental_id', type=int, required=True, help='Rental ID cannot be blank!')
parser.add_argument('score', type=int, required=True, help='Score cannot be blank!')


class MovieRatingController(Resource):
    def get(self, movie_id):
        ratings = db.session.execute(db.select(Rating).filter_by(movie_id=movie_id)).scalars()
        scores = [rating.score for rating in ratings]
        return {"scores": scores, "average": round(sum(scores) / len(scores), 2)}

    @check_rental_exists
    # @marshal_with(rating_fields)
    def post(self, movie_id):
        args = parser.parse_args()
        rental_id = args['rental_id']
        user_id = args['user_id']
        score = args['score']
        rental = db.session.execute(db.select(Rental).filter_by(id=rental_id)).scalar_one()
        if not rental:
            return {"status": f"Rental with ID {rental_id} not found, cannot set rating"}, 400
        if rental.user.id != args['user_id']:
            return {"status": f"Rental with ID {rental_id} was not created for user {user_id}, cannot set rating"}, 400
        if score < 1 or score > 10:
            return {"status": "Rating score must be between 1 and 10"}, 400
        ratings_for_movie_by_user = db.session.execute(
            db.select(Rating).filter_by(movie_id=movie_id, user_id=user_id)).scalar_one_or_none()
        if ratings_for_movie_by_user:
            return {"status": f"User with ID {user_id} has already rated the movie with ID {movie_id}"}, 400
        rating = Rating(score=score, user_id=user_id, movie_id=movie_id)
        db.session.add(rating)
        db.session.commit()
        return {"status": f"Rating for movie ID {movie_id} set to {score} by user ID {user_id}"}, 201
