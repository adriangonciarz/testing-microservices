from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restful import Api

from controllers.movie_ratings import MovieRatingController
from controllers.movies import MovieController, MovieListController
from controllers.rental_payments import RentalPaymentController
from controllers.rentals import RentalController, RentalListController
from controllers.users import UserListController, UserController
from database import db

app = Flask(__name__)
cors = CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)
migrate = Migrate(app, db, render_as_batch=True)
db.init_app(app)

api.add_resource(UserListController, '/api/users')
api.add_resource(UserController, '/api/users/<int:user_id>')
api.add_resource(MovieListController, '/api/movies')
api.add_resource(MovieController, '/api/movies/<int:movie_id>')
api.add_resource(MovieRatingController, '/api/movies/<int:movie_id>/rating')
api.add_resource(RentalListController, '/api/rentals')
api.add_resource(RentalController, '/api/rentals/<int:rental_id>')
api.add_resource(RentalPaymentController, '/api/rentals/<int:rental_id>/pay')


@app.route("/")
def hello_world():
    return """
            <h1> Welcome to Movie Rentals API</h1>
            <h3>Explore the endpoints</h3>
            <ul>
                <li><a href='http://127.0.0.1:8000/api/movies'>Movies</li>
                <li><a href='http://127.0.0.1:8000/api/users'>Users</li>
                <li><a href='http://127.0.0.1:8000/api/rentals'>Rentals</li>
            </ul>
            """


if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True, host='0.0.0.0', port=8000)
