from pydantic import BaseModel


class UserCreate(BaseModel):
    email: str
    name: str


class User(UserCreate):
    id: int

    class Config:
        orm_mode = True
