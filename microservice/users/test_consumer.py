from __future__ import annotations

import logging
from pathlib import Path

from http import HTTPStatus
from typing import Generator

import pytest
from pact import Consumer, Format, Like, Provider
from yarl import URL

log = logging.getLogger(__name__)

MOCK_URL = URL("http://localhost:8080")

from typing import Any, Dict

import requests


class UserConsumer:
    def __init__(self, base_uri: str) -> None:
        self.base_uri = base_uri

    def get_user(self, user_id: int) -> dict:
        uri = f"{self.base_uri}/users/{user_id}"
        response = requests.get(uri, timeout=5)
        response.raise_for_status()
        data: Dict[str, Any] = response.json()
        return data


@pytest.fixture()
def user_consumer() -> UserConsumer:
    return UserConsumer(str(MOCK_URL))


@pytest.fixture(scope="module")
def pact():
    consumer = Consumer("UserConsumer")
    pact_dir = Path(__file__).parent.resolve() / "pacts"
    pact = consumer.has_pact_with(
        Provider("UsersAPI"),
        pact_dir=pact_dir,
        publish_to_broker=True,
        host_name=MOCK_URL.host,
        port=MOCK_URL.port,
        broker_base_url="http://localhost:9292",
    )

    pact.start_service()
    yield pact
    pact.stop_service()


def test_get_unknown_user(pact, user_consumer) -> None:
    expected = {"detail": "User not found"}

    (
        pact.given("user 123 doesn't exist")
        .upon_receiving("a request for user 123")
        .with_request("get", "/users/123")
        .will_respond_with(404, body=Like(expected))
    )

    with pact:
        with pytest.raises(requests.HTTPError) as excinfo:
            user_consumer.get_user(123)
        assert excinfo.value.response is not None
        assert excinfo.value.response.status_code == HTTPStatus.NOT_FOUND
        pact.verify()
