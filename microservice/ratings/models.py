from sqlalchemy import Column, Integer

from .database import Base


class Rating(Base):
    __tablename__ = "ratings"

    id = Column(Integer, primary_key=True)
    movie_id = Column(Integer)
    user_id = Column(Integer)
    score = Column(Integer)
