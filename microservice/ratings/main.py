import json
import logging
import os

import requests
from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from kafka import KafkaProducer
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .config import kafka_brokers, rentals_api_url, ratings_topic_name
from .database import SessionLocal, engine

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logging.basicConfig(
    level=os.environ.get('LOGLEVEL', 'INFO').upper()
)

producer = KafkaProducer(bootstrap_servers=kafka_brokers, value_serializer=lambda v: json.dumps(v).encode('utf-8'))


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/ratings", response_model=schemas.Rating)
def create_rating(incoming_rating: schemas.RatingIncoming, db: Session = Depends(get_db)):
    rental_id = incoming_rating.rental_id
    rental_response = requests.get(f"{rentals_api_url}/rentals/{rental_id}")
    if rental_response.status_code == 404:
        raise HTTPException(status_code=400, detail=f"Rental {rental_id} was not found")
    user_id, movie_id = rental_response.json()["user_id"], rental_response.json()["movie_id"]
    previous_ratings = crud.get_rating_for_user_movie(db=db, user_id=user_id, movie_id=movie_id)
    if previous_ratings:
        raise HTTPException(status_code=400, detail=f"Movie {movie_id} was already rated by user {user_id}")
    rating = schemas.RatingCreate(movie_id=movie_id, user_id=user_id, score=incoming_rating.score)
    producer.send(topic=ratings_topic_name,
                  value={"movie_id": movie_id, "user_id": user_id, "score": rating.score})
    return crud.create_rating(db=db, rating=rating)


@app.get("/ratings", response_model=list[schemas.Rating])
def read_rentals(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_ratings(db, skip=skip, limit=limit)
