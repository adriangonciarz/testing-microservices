import os

kafka_brokers = os.getenv("KAFKA_BROKERS", "127.0.0.1:9092")
rentals_api_url = os.getenv("RENTALS_API_URL")
ratings_topic_name = os.getenv("RATINGS_TOPIC_NAME", "movie-ratings")