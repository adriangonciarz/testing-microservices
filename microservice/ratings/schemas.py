from pydantic import BaseModel


class RatingIncoming(BaseModel):
    rental_id: int
    score: int


class RatingCreate(BaseModel):
    movie_id: int
    user_id: int
    score: int


class Rating(RatingCreate):
    id: int

    class Config:
        orm_mode = True
