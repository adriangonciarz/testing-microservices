from sqlalchemy.orm import Session

from . import models, schemas


def get_rating(db: Session, rating_id: int) -> models.Rating:
    return db.query(models.Rating).filter(models.Rating.id == rating_id).first()


def get_rating_for_user_movie(db: Session, user_id: int, movie_id: int) -> models.Rating:
    return db.query(models.Rating).filter(
        models.Rating.user_id == user_id, models.Rating.movie_id == movie_id).first()


def get_ratings(db: Session, skip: int = 0, limit: int = 100) -> [models.Rating]:
    return db.query(models.Rating).offset(skip).limit(limit).all()


def create_rating(db: Session, rating: schemas.RatingCreate) -> models.Rating:
    db_rating = models.Rating(user_id=rating.user_id, movie_id=rating.movie_id, score=rating.score)
    db.add(db_rating)
    db.commit()
    db.refresh(db_rating)
    return db_rating
