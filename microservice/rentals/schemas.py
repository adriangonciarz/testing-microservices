import datetime

from pydantic import BaseModel


class RentalPayment(BaseModel):
    credit_card_number: str


class RentalCreate(BaseModel):
    movie_id: int
    user_id: int
    start_date: datetime.datetime
    end_date: datetime.datetime


class Rental(RentalCreate):
    id: int
    paid: bool

    class Config:
        orm_mode = True
