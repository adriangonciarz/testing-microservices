import os

payments_lambda_url = "https://lv1nktikne.execute-api.eu-central-1.amazonaws.com/default/test-lambda"
users_api_url = os.getenv("USERS_API_URL")
movies_api_url = os.getenv("MOVIES_API_URL")
log_level = os.environ.get('LOGLEVEL', 'INFO').upper()