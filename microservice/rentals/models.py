from sqlalchemy import Column, Integer, DateTime, Boolean

from .database import Base


class Rental(Base):
    __tablename__ = "rentals"

    id = Column(Integer, primary_key=True)
    movie_id = Column(Integer)
    user_id = Column(Integer)
    start_date = Column(DateTime)
    end_date = Column(DateTime)
    paid = Column(Boolean, default=False)