from sqlalchemy.orm import Session

from . import models, schemas


def get_rental(db: Session, rental_id: int) -> models.Rental:
    return db.query(models.Rental).filter(models.Rental.id == rental_id).first()


def get_rentals(db: Session, skip: int = 0, limit: int = 100) -> [models.Rental]:
    return db.query(models.Rental).offset(skip).limit(limit).all()


def create_rental(db: Session, rental: schemas.RentalCreate) -> models.Rental:
    db_rental = models.Rental(user_id=rental.user_id, movie_id=rental.movie_id, start_date=rental.start_date,
                              end_date=rental.end_date)
    db.add(db_rental)
    db.commit()
    db.refresh(db_rental)
    return db_rental

def mark_rental_as_paid(db: Session, rental: schemas.RentalCreate) -> models.Rental:
    rental.paid = True
    db.commit()
    db.refresh(rental)
    return rental
