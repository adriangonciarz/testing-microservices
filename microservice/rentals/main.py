import logging

import requests
from fastapi import Depends, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .config import movies_api_url, users_api_url, payments_lambda_url, log_level
from .database import SessionLocal, engine

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logging.basicConfig(
    level=log_level
)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/rentals", response_model=schemas.Rental)
def create_rental(rental: schemas.RentalCreate, db: Session = Depends(get_db)):
    user_response = requests.get(f"{users_api_url}/users/{rental.user_id}")
    if user_response.status_code != 200:
        raise HTTPException(status_code=400, detail=f"User {rental.user_id} not found")
    movie_response = requests.get(f"{movies_api_url}/movies/{rental.movie_id}")
    if movie_response.status_code != 200:
        raise HTTPException(status_code=400, detail=f"Movie {rental.movie_id} not found")
    return crud.create_rental(db=db, rental=rental)


@app.get("/rentals", response_model=list[schemas.Rental])
def read_rentals(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_rentals(db, skip=skip, limit=limit)


@app.get("/rentals/{rental_id}", response_model=schemas.Rental)
def read_rental(rental_id: int, db: Session = Depends(get_db)):
    db_rental = crud.get_rental(db, rental_id=rental_id)
    if db_rental is None:
        raise HTTPException(status_code=404, detail="Rental not found")
    return db_rental


@app.post("/rentals/{rental_id}/pay", response_model=schemas.Rental)
def pay_for_rental(rental_id, payment: schemas.RentalPayment, db: Session = Depends(get_db)):
    db_rental = crud.get_rental(db, rental_id=rental_id)
    if db_rental.paid:
        raise HTTPException(status_code=400, detail="Rental already paid")
    payment_response = requests.post(payments_lambda_url, json={"cc_number": payment.credit_card_number})
    logging.debug(f"Payment Lambda response: {payment_response.status_code}, {payment_response.text}")
    if payment_response.status_code == 200:
        return crud.mark_rental_as_paid(db, db_rental)
    else:
        raise HTTPException(status_code=400, detail="Payment failed")
