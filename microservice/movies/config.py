import os

kafka_brokers = os.getenv("KAFKA_BROKERS", "127.0.0.1:9092")
ratings_topic_name = os.getenv("RATINGS_TOPIC_NAME", "movie-ratings")