from sqlalchemy.orm import Session

from . import models, schemas


def get_movie(db: Session, movie_id: int):
    return db.query(models.Movie).filter(models.Movie.id == movie_id).first()


def get_movie_by_title(db: Session, movie_title: int):
    return db.query(models.Movie).filter(models.Movie.title == movie_title).first()


def get_movies(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Movie).offset(skip).limit(limit).all()


def create_movie(db: Session, movie: schemas.MovieCreate):
    db_movie = models.Movie(title=movie.title, info=movie.info, release_year=movie.release_year,
                            renting_price=movie.renting_price)
    db.add(db_movie)
    db.commit()
    db.refresh(db_movie)
    return db_movie
