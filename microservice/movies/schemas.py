from pydantic import BaseModel


class MovieCreate(BaseModel):
    title: str
    info: str
    release_year: int
    renting_price: float


class Movie(MovieCreate):
    id: int

    class Config:
        orm_mode = True
