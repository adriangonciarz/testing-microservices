from sqlalchemy import Column, Integer, String, Float

from .database import Base


class Movie(Base):
    __tablename__ = "movies"

    id = Column(Integer, primary_key=True)
    title = Column(String, unique=True, index=True)
    info = Column(String)
    release_year = Column(Integer)
    renting_price = Column(Float)
