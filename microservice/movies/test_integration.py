import uuid

import pytest
from httpx import AsyncClient

from .main import app


@pytest.fixture
def anyio_backend():
    return 'asyncio'


@pytest.mark.anyio
async def test_read_movies():
    async with AsyncClient(app=app, base_url='http://127.0.0.1:8000') as ac:
        response = await ac.get("/movies")
    assert response.status_code == 200


@pytest.mark.anyio
async def test_create_movie():
    async with AsyncClient(app=app, base_url='http://127.0.0.1:8000') as ac:
        response = await ac.post("/movies",
                                 json={"title": f"{uuid.uuid4()}",
                                       "info": "Sci-Fi",
                                       "release_year": 1978,
                                       "renting_price": 16.00
                                       })
    assert response.status_code == 200
    assert "id" in response.json()
